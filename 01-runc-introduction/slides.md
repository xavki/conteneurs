%title: RunC
%author: xavki
%blog: [Xavki Blog](https://xavki.blog)


# RunC : introduction


<br>

* création 2015 : 0.0.1

* dépôt : https://github.com/opencontainers/runc

* langage : GO

* RunC = container runtime

<br>

Imbrications et différents degrés :

runc > containerd > docker daemon > docker CLI

runc > podman

runc > crio

runc > containerd

lxc > lxd

```
docker info
```

Rq : 0 dépendance à docker

<br>

Open Container initiative

https://github.com/opencontainers/image-spec/blob/master/config.md

https://github.com/opencontainers/

----------------------------------------------------------------------------------------------------------------------------

# RunC : introduction


Popular runC features include:

```
• Linux namespaces, including user namespaces
• Security features available in Linux: Selinux, Apparmor, seccomp, control groups, capability drop, pivot_root, uid/gid dropping
• Windows 10 containers is being contributed directly by Microsoft engineers
• Arm, Intel, Qualcomm, IBM, and the entire hardware manufacturers ecosystem.
• Portable performance profiles, contributed by Google engineers based on their experience deploying containers in production.
• A formally specified configuration format, governed by the Open Container Project under the auspices ofthe Linux Foundation
````

----------------------------------------------------------------------------------------------------------

# RunC : introduction

<br>

La différence entre les CR de haut niveau et bas niveau :

* bas niveau : utilisation de OCI unqiuement (lancement de conteneur à partir d'une image OCI)
		* un fichier descriptif config.json
		* un répertoire rootfs

* haut niveau : 
		* management des images (pull/push)
		* gestion du réseau...
		* les volumes...

On parle aussi : 
		* de container runtime : sécurisation et isolation du process porté par l'image OCI
		* de container engine : gestions des images...

<br>

Autres CR de bas niveau :
* lxc
* crun : runC mais en... C
* gavisor : google
* kata-container

----------------------------------------------------------------------------------------------------------

# RunC : introduction

<br>

RunC permet de gérer, les outils internes au noyau linux : 
	* cgroup (v1/v2)
	* namespaces
	* layer capabilities : btrfs,zfs...
	* et d'autres

<br>

CGroups :
	* Réservation et limitation des ressources : CPU, mémoire, bande passante, disque, priorité

<br>

Namespaces :
	Mount : isolation par le montage de filesystem
	UTS : isolation du hostname ou domaine
	IPC : isolation des communications interprocessus
	PID : isolation par PID
	Network : isolation par les interfaces réseaux
	User : isolation par les UID et GID
	Cgroup
	(Time)

<br>

A lire absolument : https://www.grottedubarbu.fr/container-runtimes-c-est-quoi/
Egalement : https://developers.redhat.com/blog/2018/02/22/container-terminology-practical-introduction/
Ou encore : https://blog.alterway.fr/le-point-sur-les-container-runtimes.html
