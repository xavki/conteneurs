%title: RunC
%author: xavki
%blog: [Xavki Blog](https://xavki.blog)


# RunC : Conteneurs, les principes de l'OCI


<br>

* A l'origine l'OCI : https://opencontainers.org/

<br>

* Spec d'un CONTENEUR OCI :
https://github.com/opencontainers/image-spec

<br>

* Un "Filesystem bundle" > lancement d'un conteneur
		* config.json : nommage et localisation à la racine impérative
		* rootfs : le root filesystem référencé dans le root.path du config.json

Rq : structure identique si format tar...

<br>

* exemple avec docker pour retrouver le rootfs d'un conteneur nginx

```
docker export $(docker create nginx) | tar -C rootfs -xvf -
```

<br>

* ajout du config.json

```
runc run mycontainer
```

<br>

* vérification du pseudo filesystem /proc...

