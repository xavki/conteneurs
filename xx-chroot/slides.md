%title: CONTENEURS
%author: xavki
%Site : xavki.blog


# CONTENEURS : et il y avait... CHROOT

<br>
* 

  322  mkdir -p /home/test
  323  sudo mkdir -p /home/test
  324   ls -l /dev/{null,zero,stdin,stdout,stderr,random,tty}
  325  mkdir -p /home/test/dev/
  326  sudo mkdir -p /home/test/dev/
  327  cd /home/test/dev/
  328  mknod -m 666 null c 1 3
  329  sudo mknod -m 666 null c 1 3
  330  sudo mknod -m 666 tty c 5 0
  331  sudo mknod -m 666 zero c 1 5 
  332  sudo sudo mknod -m 666 random c 1 8
  333  chown root:root /home/test
  334  sudo chown root:root /home/test
  335  sudo chmod 0755 /home/test
  336  ls -ld /home/test
  337  mkdir -p /home/test/bin
  338  sudo mkdir -p /home/test/bin
  339  cp -v /bin/bash /home/test/bin/
  340  sudo cp -v /bin/bash /home/test/bin/
  341  ldd /bin/bash
  342  mkdir -p /home/test/lib64
  343  sudo mkdir -p /home/test/lib64
  344  sudo cp -v /lib64/{libtinfo.so.5,libdl.so.2,libc.so.6,ld-linux-x86-64.so.2} /home/test/lib64/
  345  sudo useradd xavki
  346  sudo passwd xavki
  347  sudo /etc/ssh/sshd_config
  348  sudo vim /etc/ssh/sshd_config
  349  sudo  systemctl restart sshd

