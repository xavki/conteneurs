%title: CONTENEURS
%author: xavki
%Site : xavki.blog


# CONTENEURS : vers l'Intermodal

<br>
* parallèle avec le transport de marchandises (maritime...)

* origines du conteneur et docker

* conteneur = support

* docker = manutentionnaire



-----------------------------------------------------------------------


# CONTENEURS : avant c'était...


<br>
* Avant 1933 :
		* transport de biens de manière hétérogène
		* sacs, barils, vrac...


<br>
* Inconvénients :
		* manutention difficile : docker = chargement à la main
		* lenteur chargement/déchargement
		* perte de place
		* stockage délicat
		* impact du changement de mode de trasnport


----------------------------------------------------------------------


# CONTENEURS : puis arriva l'idée du conteneur



<br>
* en 1933 : standardisation

* début de la mondialisation moderne

* BIC : bureau international du conteneur

* en 1967 : norme ISO 12,2m x 2,5m x 2,5m

<br>
* Avantages : 
		* gain de place
		* facilité/confort de manutention
		* vitesse chargement/déchargement
		* changement de mode facilité : camion > bateau > camion > péniche


---------------------------------------------------------------------


# CONTENEURS : et dans l'IT



<br>
* avant :
		* livraison sous différentes formes : tar, binaires (jar...)
		* dépendance aux dépendances (librairies, versions, paquets)
		* pas de standardisation = moins d'efficacité (définition du livrable)
		* stockage de manière hétérogène (répertoires, entrepôt de code...)

<br>
* après (2008 / 2013) :
		* Open Container Initiative (OCI)
		* livrable = image
		* embarque ses dépendances (attention = mises à jour)
		* abstraction du support
		* stockage facilité
		* orchestration de conteneurs : compose, swarm, kubernetes...

